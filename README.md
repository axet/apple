# MERGED

Project is merged to the https://github.com/axet/desktop

# Apple Java native functions

To build your own full featured Mac OS application with java you need to call Native Methods and use mac only java
extensions provided by Apple.

Unfortunatly those java wrappers not available on other platform, and so your java code would not build on
other platforms.

This library will allow you to write full featured Mac OSX applications using java.


## Features

  * set Dock icon float red number (like Skype or Adium does about unreded messages)
  * catchup all system events url handlers

TODO: need to replace all calls as Reflection calls

## How to build your App.app

Update your pom.xml. And write build script. Don't forget to replace:

  * main class: com.example.App
  * and version: 1.1.1

### pom.xml            
              <build>
                <plugins>
                  <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>2.4.3</version>
                    <configuration>
                      <encoding>UTF-8</encoding>
                    </configuration>
                  </plugin>
                  <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>2.3.2</version>
                    <configuration>
                      <source>1.6</source>
                      <target>1.6</target>
                      <encoding>UTF-8</encoding>
                    </configuration>
                  </plugin>
            
                  <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>1.1.1</version>
                    <configuration>
                      <archive>
                        <manifest>
                          <addClasspath>true</addClasspath>
                          <mainClass>com.example.App</mainClass>
                        </manifest>
                      </archive>
                    </configuration>
                  </plugin>
            
                  <!-- share everyting into one jar, except natives. natives we love to cook separatly -->
                  <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-shade-plugin</artifactId>
                    <version>1.6</version>
                    <executions>
                      <execution>
                        <phase>package</phase>
                        <goals>
                          <goal>shade</goal>
                        </goals>
                        <configuration>
                          <artifactSet>
                            <excludes>
                              <exclude>*:*:*:natives*</exclude>
                            </excludes>
                          </artifactSet>
                        </configuration>
                      </execution>
                    </executions>
                  </plugin>
            
                  <plugin>
                    <groupId>com.googlecode.mavennatives</groupId>
                    <artifactId>maven-nativedependencies-plugin</artifactId>
                    <version>0.0.6</version>
                    <executions>
                      <execution>
                        <id>unpacknatives</id>
                        <goals>
                          <goal>copy</goal>
                        </goals>
                      </execution>
                    </executions>
                  </plugin>
            
                </plugins>
            
              </build>
### Makefile
        
        mac: build
                rm -rf target/App.app
                mkdir -p target/App.app/Contents/MacOS
                mkdir -p target/App.app/Contents/Resources/Java
                ln -s /System/Library/Frameworks/JavaVM.framework/Resources/MacOS/JavaApplicationStub target/App.app/Contents/MacOS/
                cp mac/Info.plist target/App.app/Contents/
                cp target/mircle-*.jar target/App.app/Contents/Resources/Java/mircle.jar
                cp -r target/natives target/App.app/Contents/Resources/Java

### Info.plist
            
            <?xml version="1.0" encoding="UTF-8"?>
            <!DOCTYPE plist SYSTEM "file://localhost/System/Library/DTDs/PropertyList.dtd">
            <plist version="1.0">
              <dict>
                <key>LSMultipleInstancesProhibited</key>
                <true />
                <key>CFBundleIdentifier</key>
                <string>com.google.code.mircle</string>
                <key>CFBundleName</key>
                <string>Mircle</string>
                <key>CFBundleVersion</key>
                <string>0.0.4-SNAPSHOT</string>
                <key>CFBundleAllowMixedLocalizations</key>
                <string>true</string>
                <key>CFBundleExecutable</key>
                <string>JavaApplicationStub</string>
                <key>CFBundleDevelopmentRegion</key>
                <string>English</string>
                <key>CFBundlePackageType</key>
                <string>APPL</string>
                <key>CFBundleSignature</key>
                <string>????</string>
                <key>CFBundleInfoDictionaryVersion</key>
                <string>6.0</string>
                <key>CFBundleIconFile</key>
                <string>GenericJavaApp.icns</string>
                <key>CFBundleDocumentTypes</key>
                <array>
                  <dict>
                    <key>CFBundleTypeExtensions</key>
                    <array>
                      <string>torrent</string>
                    </array>
                    <key>CFBundleTypeName</key>
                    <string>BitTorrent Document</string>
                    <key>CFBundleTypeRole</key>
                    <string>Viewer</string>
                    <key>LSHandlerRank</key>
                    <string>Owner</string>
                  </dict>
                </array>
                <key>CFBundleURLTypes</key>
                <array>
                  <dict>
                    <key>CFBundleURLName</key>
                    <string>mirlce URL</string>
                    <key>CFBundleURLSchemes</key>
                    <array>
                      <string>mircle</string>
                    </array>
                  </dict>
                  <dict>
                    <key>CFBundleURLName</key>
                    <string>magnet URL</string>
                    <key>CFBundleURLSchemes</key>
                    <array>
                      <string>magnet</string>
                    </array>
                  </dict>
                </array>
                <key>Java</key>
                <dict>
                  <key>MainClass</key>
                  <string>com.google.code.mircle.App</string>
                  <key>JVMVersion</key>
                  <string>1.4+</string>
                  <key>WorkingDirectory</key>
                  <string>${workingDirectory}</string>
                  <key>ClassPath</key>
                  <array><string>$JAVAROOT/mircle.jar</string></array>
                </dict>
              </dict>
            </plist>

## Central Maven Repo

        <dependency>
          <groupId>com.github.axet</groupId>
          <artifactId>apple</artifactId>
          <version>0.0.2</version>
        </dependency>

